import asyncio
import discord
from discord.ext import commands

# Database index legend
BOOST_ROLES_USER_ID = 0
BOOST_ROLES_ROLE_ID = 1

DAY_IN_SECONDS = 24*60*60

class Boost(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        self.bot.loop.create_task(self.initialize_boost_roles())

        self.check_boost_roles_listener = self.bot.loop.create_task(
            self.check_boost_roles())

    def __exit__(self, exc_type, exc_value, traceback):
        """To cancel any running tasks"""
        self.check_boost_roles_listener.cancel()

    async def initialize_boost_roles(self):
        query = 'CREATE TABLE IF NOT EXISTS boost_roles (' \
                'user_id INTEGER PRIMARY KEY, ' \
                'role_id INTEGER)'
        await self.bot.db.execute(query)
        await self.bot.db.commit()

    async def check_boost_roles(self):
        """Checks if any users with the boost role have since stopped boosting every day"""
        query = 'SELECT user_id, role_id FROM boost_roles'
        while True:
            await asyncio.sleep(DAY_IN_SECONDS)

            async with self.bot.db.execute(query) as cursor:
                boost_roles = await cursor.fetchall()

                for (user_id, role_id) in boost_roles:
                    for guild in self.bot.guilds:
                        member = guild.get_member(user_id)

                        if member and not member.premium_since:
                            role = guild.get_role(role_id)

                            if role:
                                await member.remove_roles(role)
                                query = 'DELETE FROM boost_roles ' \
                                        'WHERE user_id = ?'
                                await self.bot.db.execute(query, [user_id])
                                await self.bot.db.commit()
                                await role.delete()
                                return

    @commands.group()
    async def boost(self, ctx):
        """Lets users who boost the server to give themselves a custom colour, and remove it"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(ctx.command)

    @boost.command(aliases=['color'])
    async def colour(self, ctx, colour: discord.Colour):
        if ctx.author.premium_since:
            query = 'SELECT user_id, role_id FROM boost_roles'
            async with self.bot.db.execute(query) as cursor:
                boost_roles = await cursor.fetchall()

                role_id = [role_id for (user_id, role_id) in boost_roles if user_id == ctx.author.id]
                if role_id != []:
                    role = ctx.channel.guild.get_role(role_id[0])
                    await role.edit(colour=colour)
                    await ctx.send('Your role has been updated')
                else:
                    role = await ctx.channel.guild.create_role(
                        name=ctx.author.name,
                        colour=colour
                    )
                    await role.edit(position=ctx.channel.guild.premium_subscriber_role.position + 1)
                    await ctx.author.add_roles(role)

                    query = 'INSERT INTO boost_roles VALUES (?, ?)'
                    await self.bot.db.execute(query, [ctx.author.id, role.id])
                    await self.bot.db.commit()

                    await ctx.send('You now have your special colour')
        else:
            await ctx.send('Boost the server to pick your colour')

    @boost.command(aliases=['removecolor'])
    async def removecolour(self, ctx):
        if ctx.author.premium_since:
            query = 'SELECT role_id from boost_roles WHERE user_id = ?'

            async with self.bot.db.execute(query, [ctx.author.id]) as cursor:
                role_id = await cursor.fetchall()
                try:
                    role = ctx.channel.guild.get_role(role_id[0][0])
                    if role:
                        await ctx.author.remove_roles(role)
                        query = 'DELETE FROM boost_roles ' \
                                'WHERE user_id = ?'
                        await self.bot.db.execute(query, [ctx.author.id])
                        await self.bot.db.commit()
                        await role.delete()

                        await ctx.send('Your colour has been removed')
                        return
                except IndexError:
                    pass
            await ctx.send('Failed to remove colour, you may not have one, or something went wrong')

def setup(bot):
    bot.add_cog(Boost(bot))
