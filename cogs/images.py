import json
from random import shuffle
from random import choice as rndchoice

import discord
from discord.ext import commands


class Images(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.cleanConverter = discord.ext.commands.clean_content()

        # Opening all the image data
        with open('data/images.json') as data:
            self.imageData = json.load(data)

        self.images = {}

        # Load all images
        self.images['blush'] = self.imageData['blushImages']
        self.images['hug'] = self.imageData["hugImages"]
        self.images['kiss'] = self.imageData["kissImages"]
        self.images['pat'] = self.imageData["patImages"]
        self.images['pout'] = self.imageData["poutImages"]
        self.images['slap'] = self.imageData["slapImages"]

        # prepare tetris-bag-random buffer
        self.buffer = {}
        for k in self.images:
            self.buffer[k] = self.images[k].copy()
            shuffle(self.buffer[k])

    async def helper(self, ctx, category, users=None):
        # check if tetris-bag empty
        if len(self.buffer[category]) <= 0:
            self.buffer[category] = self.images[category].copy()
            shuffle(self.buffer[category])

        # pick random image
        choice = rndchoice(self.buffer[category])
        self.buffer[category].remove(choice)

        if users is None:
            await ctx.send(choice)
        else:
            await ctx.send(' '.join([user.mention for user in users]) + " " + choice)

    # TODO: use .format() to merge thes into 1 or 2 commands
    # TODO: use @commands.help() for help text
    @commands.command()
    async def blush(self, ctx):
        """Shows a blush"""
        await self.helper(ctx, 'blush')

    @commands.command()
    async def pout(self, ctx):
        """Shows a pout"""
        await self.helper(ctx, 'pout')

    @commands.command()
    async def hug(self, ctx, *users: discord.Member):
        """Mentions the user with an image of a hug"""
        await self.helper(ctx, 'hug', users)

    @commands.command()
    async def kiss(self, ctx, *users: discord.Member):
        """Mentions the user with an image of a kiss"""
        await self.helper(ctx, 'kiss', users)

    @commands.command()
    async def pat(self, ctx, *users: discord.Member):
        """Mentions the user with an image of a pat"""
        await self.helper(ctx, 'pat', users)

    @commands.command()
    async def slap(self, ctx, *users: discord.Member):
        """Mentions the user with an image of a slap"""
        await self.helper(ctx, 'slap', users)


def setup(bot):
    bot.add_cog(Images(bot))
