#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

import aiosqlite
import asyncio
import json
import logging
import re
import textwrap
import util

import discord
from discord.ext import commands

description = 'Cute Discord Bot, please don\'t bully'

bot = commands.Bot(command_prefix=util.config['prefix'],
                    description=description,
                    intents=discord.Intents.all())

def load_default_cogs():
    # Loads all startup extensions into the bot
    with open('data/cogs.json') as data:
        cogs = json.load(data)
        for cog in cogs:
            try:
                bot.load_extension(cog)
            except commands.ExtensionAlreadyLoaded:
                print(f'Could not load extension {cog} (already loaded)')
            except commands.ExtensionNotLoaded:
                print(f'Could not load extension {cog} (not loaded)')
            except commands.NoEntryPointError:
                print(f'Could not load extension {cog} (no entrypoint)')
            except commands.ExtensionFailed as fail:
                print(f'Could not load extension {cog} (startup error):')
                print(fail)
            except Exception as e:
                print(f'Could not load extension {cog} (unknown error)')
                print(e)

async def create_db_connection():
    """Creates a connection to the database"""
    try:
        bot.db = await aiosqlite.connect('data/browniebot.db')
    except aiosqlite.error as e:
        print(f'Failed to connect to database: {e}')

async def start_bot(token: str = None):
    try:
        await create_db_connection()
        load_default_cogs()
        await bot.start(token)
    except KeyboardInterrupt:
        print('Logging off')
        await bot.db.close()
        await bot.logout()

@bot.event
async def on_ready():
    print(textwrap.dedent(
        f"""
        Logged in as:
        {bot.user.name}
        {bot.user.id}
        ------------------
        """
    ))

@bot.event
async def on_message(message):
    """Handles any messages detected by the bot"""
    # Ignores the bot's messages
    if message.author == bot.user:
        return

    await bot.process_commands(message)

def user_is_elevated(ctx):
    # TODO: replace magic word 'Sensei' with variable
    # TODO: add 'commands.check_any' helper?
    if commands.check(commands.is_owner()) or commands.has_role('Sensei'):
        return True

def is_valid(self, userId: int = None):
    """Checks if the user is one I've allowed to use special commands"""
    if userId == util.config['creatorId']:
        return True

    if userId in util.config['validId']:
        return True

    return False

commands.Bot.is_valid = is_valid

def get_user(self, ctx, username):
    """To find the given user"""
    if username:
        # Checks if there exists a user named 'username'
        user = ctx.guild.get_member_named(username)
        if not user:
            # Converts the user to an id
            try:
                user = ctx.guild.get_member(
                    int(re.sub('[^0-9]', '', username)))
            except Exception as e:
                print(e)
                user = None
    else:
        user = ctx.author

    return user

commands.Bot.getUser = get_user

# Setting the clean converter as a member variable
commands.Bot.clean_converter = discord.ext.commands.clean_content()

if __name__ == '__main__':
    # Setting up logging
    logger = logging.getLogger('discord')
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(
        filename='discord.log', encoding='utf-8', mode='a')
    handler.setFormatter(logging.Formatter(
        '%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
    logger.addHandler(handler)

    # Running the bot
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start_bot(util.config['token']))
