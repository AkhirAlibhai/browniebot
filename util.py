import os
import json

base_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(base_path)

with open('data/config.json') as data:
    config = json.load(data)
